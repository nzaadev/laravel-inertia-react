import { defineConfig } from 'vite';
import laravel from 'laravel-vite-plugin';
import react from '@vitejs/plugin-react';

// https://github.com/laravel/vite-plugin/blob/main/UPGRADE.md#configure-vite
export default defineConfig({
    plugins: [
        laravel([]),
        react(),
    ],
    resolve: {
        alias: {
            '@': '/resources/js'
        }
    }
});
