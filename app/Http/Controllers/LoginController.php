<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Inertia\Inertia;

class LoginController extends Controller
{
    function index() {
        return Inertia::render('Login');
    }

    /**
     * Handle an authentication attempt.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function authenticate(Request $request)
    {
        $data =  json_decode($request->getContent(), true);
        $rules = [
            'email' => 'required',
            'password' => 'required'
        ];
        $validator = Validator::make($data, $rules);
        if($validator->fails()) {
            $arr = $validator->errors();
            $new =['error' => true, 'errors' => $arr];
            return response()->json($new);
        } else {
            $data = $validator->validated();
            if(Auth::attempt($data)) {
                return response()->json(['message' => 'user success login', 'error' => false]);
            } else {
                $new =['error' => true, 'errors' => ['email' => ['Error data not found']]];
                return response()->json($new);
            }
        }
    }
}
