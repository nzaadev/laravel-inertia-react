<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Auth\Events\Validated;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Inertia\Inertia;

class RegisterController extends Controller
{
    function authenticate(Request $request) {
        $data =  json_decode($request->getContent(), true);
        $rules = [
            'name' => 'required',
            'email' => 'required|email|unique:users,email',
            'password' => 'required|confirmed',
        ];
        $validator = Validator::make($data, $rules);
        if($validator->fails()) {
            $arr = $validator->errors();
            $new =['error' => true, 'errors' => $arr];
            return response()->json($new);
        } else {
            $data = $validator->validated();
            $user = new User();
            $user->name = $data['name'];
            $user->password = bcrypt($data['password']);
            $user->email = $data['email'];
            $user->email_verified_at =  date("Y-m-d H:i:s");
            $user->save();
            return response()->json(['message' => 'user success register', 'error' => false]);
        }


    }

    function index() {
        return Inertia::render('Register');
    }
}
