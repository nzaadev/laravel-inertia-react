import React from 'react'
import  Header  from '../components/Header'
import { Hero } from '../components/Hero'
import { usePage } from '@inertiajs/react'

export default function Index() {
    const { auth } = usePage().props
    return (
    <>
        <Header auth={auth}/>
        <Hero/>
    </>
    )
}
