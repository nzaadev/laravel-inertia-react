import React from 'react'
import { usePage } from '@inertiajs/react'
import Headers from '../components/Header'
import { Card } from 'react-bootstrap'

const Dashboard = () => {

    const { auth } = usePage().props
    return (
        <div>
            <Headers auth={auth}/>
            <div className="container">
                <h1 className='mt-5'>Dashboard</h1>
                <Card className='mt-5'>
                    <Card.Header className='fw-bold'>Card Header</Card.Header>
                    <Card.Body>
                        Lorem ipsum dolor sit amet consectetur adipisicing elit. Sint odio, dolores veniam molestias, iure quo maiores eum dolor iste odit alias autem corporis porro minima vel incidunt maxime sapiente soluta.
                    </Card.Body>
                </Card>
            </div>
        </div>
    )
}

export default Dashboard
