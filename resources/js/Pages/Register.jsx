import { Link, router, usePage } from '@inertiajs/react';
import axios from 'axios';
import React, { useEffect, useState } from 'react'
import { Form, Button, Card } from 'react-bootstrap';

const Register = () => {


    const { auth } = usePage().props
    const [error, setError] = useState({})

    useEffect(() => {
        if (auth.user != undefined) {
            router.visit("/dashboard")
            return
        }
    }, [])

    const handleRegister = (e) => {
        e.preventDefault()
        console.log(e.target.password.value);
        // return
        axios.post('/register',
            JSON.stringify(
                {
                    name: e.target.name.value,
                    email: e.target.email.value,
                    password: e.target.password.value,
                    password_confirmation: e.target.password_confirmation.value,
                }), {
            headers: {
                "Content-Type": "application/json"
            }
        }
        ).then(({ data }) => {
            if (data.error) {
                console.log(typeof data.errors);
                setError(data.errors)
            } else {
                router.visit("/login")
            }
        }).catch(res => {
            console.log("err", res);
        })
    }

    if (auth.user) {
        return
    }

    return (

        <div className="row container mx-auto mt-5 mt-md-3 mt-lg-5">
            <Card className='col-sm mx-auto col-md-7 col-lg-6'>
                <Card.Body>

                    <h2>Register</h2>
                    <p className='mb-3'>Register here</p>

                    <Form onSubmit={handleRegister}>
                        <Form.Group className="mb-3">
                            <Form.Label htmlFor='name'>Name</Form.Label>
                            <Form.Control id='name' type="text" placeholder="Enter name" name='name' className={`${error.name && "is-invalid"}`} />
                            {error.email && error.name.map((e, i) => <Form.Text key={i} className='invalid-feedback'>{e}</Form.Text>)}
                        </Form.Group>

                        <Form.Group className="mb-3">
                            <Form.Label htmlFor='email'>Email address</Form.Label>
                            <Form.Control id='email' type="email" placeholder="Enter email" name='email' className={`${error.email && "is-invalid"}`} />
                            {error.email && error.email.map((e, i) => <Form.Text key={i} className='invalid-feedback'>{e}</Form.Text>)}
                        </Form.Group>

                        <Form.Group className="mb-3">
                            <Form.Label htmlFor='password'>Password</Form.Label>
                            <Form.Control id='password' type="password" name='password' placeholder="Password" className={`${error.password && "is-invalid"}`} />
                            {error.password && error.password.map((e, i) => <Form.Text key={i} className='invalid-feedback'>{e}</Form.Text>)}
                        </Form.Group>

                        <Form.Group className="mb-3">
                            <Form.Label htmlFor='password_confirmation'>Password Confirm</Form.Label>
                            <Form.Control id='password_confirmation' type="password" name='password_confirmation' placeholder="Password confirm" />
                        </Form.Group>

                        {/* <Form.Group className="mb-3" controlId="formBasicCheckbox">
                            <Form.Check type="checkbox" label="Check me out" />
                        </Form.Group> */}
                        <Button className='w-100' variant="primary" type="submit">
                            Submit
                        </Button>

                        <p className='mt-2'>Already have account? <Link href="/login">Login here</Link></p>

                        <p className="text-muted">&copy; NZDev {new Date().getFullYear()}</p>

                    </Form>
                </Card.Body>
            </Card>
        </div>
    )


}

export default Register
