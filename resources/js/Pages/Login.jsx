import { Link, router, usePage } from '@inertiajs/react';
import axios from 'axios';
import React, { useEffect, useState } from 'react'
import { Form, Button, Card } from 'react-bootstrap';

const Login = () => {

    const { auth } = usePage().props
    const [error, setError] = useState({})

    useEffect(() => {
        if(auth.user != undefined) {
            router.visit("/dashboard")
            return
        }
    }, [])

    const handleLogin = (e) => {

        e.preventDefault()
        axios.post('/login',
            JSON.stringify(
                {
                    email: e.target.email.value,
                    password: e.target.password.value,
                }), {
                    headers: {
                        "Content-Type": "application/json"
                    }
                }
            ).then(({data}) => {
                if(data.error) {
                    setError(data.errors)
                } else {
                    router.visit("/dashboard")
                }
            }).catch(res => {
                console.log("err", res);
            })
    }

    if(auth.user) {
        return
    }

    return (

        <div className="row container mx-auto mt-5 mt-md-3 mt-lg-5">
            <Card className='col-sm mx-auto col-md-7 col-lg-6'>
                <Card.Body>

                    <h2>Login</h2>
                    <p className='mb-3'>Login here</p>

                    <Form onSubmit={handleLogin}>

                        <Form.Group className="mb-3">
                            <Form.Label htmlFor='email'>Email address</Form.Label>
                            <Form.Control id='email' type="email" placeholder="Enter email" name='email' className={`${error.email && "is-invalid"}`} />
                            {error.email && error.email.map((e, i) => <Form.Text key={i} className='invalid-feedback'>{e}</Form.Text>)}
                        </Form.Group>

                        <Form.Group className="mb-3">
                            <Form.Label htmlFor='password'>Password</Form.Label>
                            <Form.Control id='password' type="password" name='password' placeholder="Password" className={`${error.password && "is-invalid"}`} />
                            {error.password && error.password.map((e, i) => <Form.Text key={i} className='invalid-feedback'>{e}</Form.Text>)}
                        </Form.Group>

                        <Button className='w-100' variant="primary" type="submit">
                            Login
                        </Button>

                        <p className='mt-2'>No have account? <Link href="/register">Register here</Link></p>

                        <p className="text-muted">&copy; NZDev {new Date().getFullYear()}</p>

                    </Form>
                </Card.Body>
            </Card>
        </div>
    )

}

export default Login
