import React from 'react'

export const Hero = () => {
    return (
        <>
            <div className='container  d-flex flex-column flex-lg-row mt-2 mx-lg-auto mt-md-5 align-items-center'>
                <div className='order-1 order-lg-0'>
                    <h1 className='hero-title fw-bolder'>Lorem ipsum dolor sit amet consectetur adipisicing elit. Fugiat, minima.</h1>
                    <p className='hero-desc'>Lorem ipsum dolor sit amet consectetur adipisicing elit. Aperiam iste totam maiores veniam expedita officia voluptate, fuga magnam, asperiores possimus ipsum aut non in quis. Eius quas, ea illo natus repudiandae corrupti assumenda tenetur tempore sunt dolores architecto provident accusamus pariatur ab at nostrum neque omnis officiis ipsam esse! Maxime, fuga asperiores eaque, omnis corporis suscipit quam dolorem dolores sit excepturi corrupti beatae. Ad at nihil omnis necessitatibus? Id debitis tempora accusamus tenetur consectetur sint! Saepe est aliquam debitis cupiditate ipsam.</p>
                    <div>
                        <button className='btn shadow-sm btn-primary' style={{ marginRight: "10px" }}>Get Started</button>
                        <button className='btn shadow-sm btn-outline-primary'>Contact Me</button>
                    </div>
                </div>

                <div>
                    <img className='hero-img order-0 order-lg-1' src="/programmer.png" alt="" />
                </div>
            </div>
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1440 320"><path fill="#0d6efd" fill-opacity="0.75" d="M0,160L1440,96L1440,320L0,320Z"></path></svg>
        </>
    )
}
