import React from 'react'
import { createInertiaApp } from '@inertiajs/react'
import { createRoot } from 'react-dom/client'
import { resolvePageComponent } from 'laravel-vite-plugin/inertia-helpers';
import 'bootstrap/dist/css/bootstrap.min.css'


createInertiaApp({
    progress: {
        color: '#ff0000',
        includeCSS: true,
        showSpinner: true,
    },
    // https://github.com/inertiajs/inertia-laravel/issues/442
    resolve: (name) => resolvePageComponent(`./Pages/${name}.jsx`, import.meta.glob('./Pages/**/*.jsx'))
    .then((module) => {
        return module.default
    }),
    setup({ el, App, props }) {
        createRoot(el).render(<App {...props} />)
    },
})

